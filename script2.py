#!/usr/bin/env python

import slugify
import sys
import os
import nltk

from contextlib import redirect_stdout
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
# from nltk.corpus import wordnet

# https://stackoverflow.com/questions/47614999/how-to-stop-nltk-from-outputting-to-terminal-when-downloading-data/47616241#47616241
# modify the sys.stdout temporarily on the fly - there is that
# redirect_stdout() context manager in Python 3.4+:
with redirect_stdout(open(os.devnull, "w")):
  nltk.download('stopwords', quiet=True)
  # nltk.download('wordnet')

def rename(name):
  None

def processFolder(path):
  cwd = os.getcwd()
  folder =
  for f in os.listdir():
    rename(f)


if __name__ == '__main__':
  try:
    path = sys.argv[1]
    processFolder(path)

  except IndexError:
    print("plz a path")
    sys.exit(1)

  except Exception:
    print('wa hapent')
    raise Exception


#!/usr/bin/env bash

# use nullglob in case there are no matching files
shopt -s nullglob

# create an array with all the files/folders in ./data/*
arr=(./data/*)

# iterate through array using a counter
for ((i=0; i<${#arr[@]}; i++)); do
    #do something to each element of array

    newname=`python -m slugify "${arr[$i]}"`
    newname=`echo $newname | cut -d'-' -f1-8 | xargs -n 1 -I {} echo {}.html`
    echo $newname
done
